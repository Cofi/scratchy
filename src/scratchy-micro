#!/usr/bin/python
# -*- coding: utf-8 -*-

#  Scratchy is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
#  Copyright 2017 Filip Danilović <filip@openmailbox.org>
#
#  Scratchy home page is: https://gitlab.com/Cofi/scratchy

# pylint: disable=invalid-name

""" Scratchy -- The simple scratchpad/note-taking program (Micro/GTK). """

from gi import require_version
require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Pango  # pylint: disable=wrong-import-position

class ScratchyMicro:  # pylint: disable=old-style-class
    """ The lone class. """
    def __init__(self):
        """ Setup main window. """
        win = Gtk.Window(title="µScratchy", border_width=4)
        win.set_size_request(480, 360)
        win.connect("destroy", self.destroy)
        self.win = win

        icon_theme = Gtk.IconTheme.get_default()
        try:
            win.set_icon(icon_theme.load_icon("accessories-text-editor", 24, 0))
            # for a proper "except" bellow we would need to import GLib,
            # which would be unused otherwise == disable the pylint warning
        except:  # pylint: disable=bare-except
            pass

        self.wrap_text = True
        font = Pango.FontDescription("Monospace 12")

        try:
            require_version("GtkSource", "3.0")
            from gi.repository import GtkSource
            self.txt = GtkSource.View(wrap_mode=Gtk.WrapMode.WORD)
        except (ImportError, ValueError):
            self.txt = Gtk.TextView(wrap_mode=Gtk.WrapMode.WORD)
            print("GtkSourceView is unavailable. Undo functionality is disabled")  # pylint: disable=superfluous-parens

        self.txt.modify_font(font)
        self.txt_buffer = self.txt.get_buffer()

        scroll = Gtk.ScrolledWindow()
        scroll.add(self.txt)

        win.connect("key-press-event", self.on_key_press)
        win.add(scroll)
        win.show_all()

    def on_key_press(self, widget, event):  # pylint: disable=unused-argument
        """ Respond to keypress events (from the main window). """
        ctrl = (event.state & Gdk.ModifierType.CONTROL_MASK)

        if ctrl:
            if event.keyval == Gdk.KEY_q:
                self.destroy(None)
            elif event.keyval == Gdk.KEY_r:
                self.txt_buffer.set_text("")
            elif event.keyval == Gdk.KEY_w:
                if self.wrap_text:
                    self.txt.set_wrap_mode(Gtk.WrapMode.NONE)
                    self.wrap_text = False
                else:
                    self.txt.set_wrap_mode(Gtk.WrapMode.WORD)
                    self.wrap_text = True

    def destroy(self, widget):  # pylint: disable=unused-argument, no-self-use
        """ Commit suicide. """
        Gtk.main_quit()

if __name__ == '__main__':
    APP = ScratchyMicro()
    APP.win.show_all()
    Gtk.main()
