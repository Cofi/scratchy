1. Use 4 space indentation
2. Keep Python 2/3 compatibility
3. Stick to [PEP8](https://www.python.org/dev/peps/pep-0008/) as much as possible
4. Optionally, though preferably, use `pylint`